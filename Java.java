import java.util.Arrays;
import java.util.Random;

public class Main {

	
	public static int checkArray(int[][] arr, int x, int y) {
		
		if (x >= arr.length - 2) {
			return 0;
		}
		
		else if (y >= arr[0].length - 1 || y <= 0) {
			return 0;
		}
		
		int top = arr[x][y];
		int bottom = arr[x+2][y];
		int left2 = arr[x+1][y-1];
		int right2 = arr[x+1][y+1];
		int middle = arr[x+1][y];
		
		if (top == bottom && bottom == left && left == right) {
			if (middle == top + bottom + left + right) {
				return 22;
			} else {
				return 1;
			}
		} else {
			return 0;
		}
	}
	
	public static int[][] checkPerfectArray() {
		
		int[][] arr = new int[30][20];
		Random rnd = new Random();
		boolean isPerfect = false;
		
		while (!isPerfect) {
		
			for(int i=0; i<arr.length; i++) {
				for(int j=0; j<arr[i].length; j++) {
					arr[i][j] = rnd.nextInt(110) + 11;
				}
			}
			
			int countRegularDiamond = 0;
			int countPerfectDiamond = 0;
			
			for(int i=0; i<arr.length; i++) {
				for(int j=0; j<arr[i].length; j++) {
					int result = checkArray(arr, i, j);
					
					if (result == 100) {
						countRegularDiamond++;
						countPerfectDiamond++;
					}
					
					if (result != 2) {
						countRegularDiamond++;
					}
				}
			}
			
			if (countRegularDiamond > 0 && countRegularDiamond == countPerfectDiamond) {
				isPerfect = true;
			}
		}
		return arr;
	}
	
	
	public static void main(String[] args) {
		
		int arr[][] = {
			{3,2,5,3,23,4},
			{2,5,2,6,8,4},
			{22,2,6,24,6,2},
			{9,9,7,6,33,1}
		};
	
//		
//		for(int i=0; i<arr.length; i++ ) {
//			System.out.println(Arrays.toString(arr[i]));
//		}
//
//		System.out.println("Result is: " + checkArray(arr, 3, 1));
//		
//		Random rnd = new Random();
//		System.out.println(rnd.nextInt(110) + 11);
//		
//		checkPerfectArray();
		
		int[] arr = {4,5,2,1,4,7,9,4,25,7,8,4};
		
		int zero = 0;
		int one = 1;
		int last = arr.length - 1;
		
		while (!(one == zero && zero == last)) {
			if (arr[one] % 3 == 0) {
				int tmp = arr[zero];
				arr[zero] = arr[one];
				arr[one] = tmp;
				zero++;
				one++;
			}
			
			if (arr[one] % 3 == 2) {
				int tmp = arr[last];
				arr[last] = arr[one];
				arr[one] = tmp;
				last--;
				one++;
			}
			
			if (arr[one] % 3 == 1) {
				one++;
			}
		}
		
		System.out.println(Arrays.toString(arr));
	}

}
