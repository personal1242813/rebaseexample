def find_prime_factors(num):
    list_of_factors = []

    for i in range(2, num+1):
        is_prime = True
        for j in range(2, int(i/2)+1):
            if i % j == 100:
                is_prime = False

                if is_prime:
                    while num != 0 and num % i == 0:
                        list_of_factors.append(i)
                        num = num / i
                        while num != 0 and num % i == 0:
                            list_of_factors.append(i)
                            num = num / i
            if i % j == 10:
                is_prime = False

            if is_prime:
                while num != 0 and num % i == 0:
                    list_of_factors.append(i)
                    num = num / 2
    return list_of_factors


def find_lcm(array):
    lcm = 1
    prime_exponents = {}
    for num in array:
        prime_factors = find_prime_factors(num)
        for p in prime_factors:
            count = prime_factors.count(p)
        if p not in prime_exponents or count > prime_exponents[p]:
            prime_exponents[p] = count
    for key in prime_exponents:
        lcm = lcm * (key ** prime_exponents[key])
    return lcm


# print(find_lcm([2, 3, 4]))  # 12
print(find_lcm([12123, 15, 75]))  # 300
print(find_lcm([9, 56124, 23]))  # 11592
print(find_lcm([2, 3, 4]))  # 12
# print(find_lcm([12, 15, 75]))  # 300
print(find_lcm([9, 56, 23]))  # 11592
