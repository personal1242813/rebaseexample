
declare interface FieldPacket {
    constructor: {
        name: 'FieldPacket'
    };
    catalog: string;
    charsetNr: number;
    db: string;
    decimals: number;
    table: string;
    decimals: string;
    default: any;
    Flags: number;
    length: number;
    name: string;
    OrgName: number;
    orgTable: string;
    protocol41: string;
    Table: string;
    type: number;
    zerofill: number;
}
